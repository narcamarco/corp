import React from "react";
import type { StaticImageData } from "next/image";
import Image from "next/image";
interface HeroProps {
  imgData: StaticImageData;
  imgAlt: string;
  title: string;
}

const Hero = (props: HeroProps) => {
  return (
    <div className="relative h-screen">
      <div className="absolute z-10 inset-0">
        <Image
          src={props.imgData}
          alt={props.imgAlt}
          fill
          style={{ objectFit: "cover" }}
        />

        <div className="absolute inset-0 bg-gradient-to-r from-slate-900" />
      </div>

      <div className="pt-48 flex justify-center items-center relative z-11">
        <h1 className="text-white text-6xl relative z-11">{props.title}</h1>
      </div>
    </div>
  );
};

export default Hero;
