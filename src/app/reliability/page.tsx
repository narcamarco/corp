import Hero from "@/components/hero";
import React from "react";
import reliabilityImage from "/public/reliability.jpg";

const ReliabilityPage = () => {
  return;
  <div>
    <Hero
      imgData={reliabilityImage}
      imgAlt="Reliability"
      title="Super High reliability hosting"
    />
  </div>;
};

export default ReliabilityPage;
