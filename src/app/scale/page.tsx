import Hero from "@/components/hero";
import React from "react";
import scaleImage from "/public/scale.jpg";

const ScalePage = () => {
  return (
    <div>
      <Hero
        imgData={scaleImage}
        imgAlt="Steel Factory"
        title="Scale your app to infinity"
      />
    </div>
  );
};

export default ScalePage;
