import Hero from "@/components/hero";
import React from "react";
import performanceImage from "/public/performance.jpg";

const PerformancePage = () => {
  return (
    <div>
      <Hero
        imgData={performanceImage}
        imgAlt="Welding"
        title="We serve high performance applications"
      />
    </div>
  );
};

export default PerformancePage;
